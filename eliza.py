import os
import random
import re

from slugify import slugify
from pygame import mixer

from gtts import gTTS
import speech_recognition as sr

from data import reflections, psychobabble

"""
sphinx; CMU Sphinx(works offline)
google_cloud; Google Cloud Speech API
wit; Wit.ai
bing; Microsoft Bing Voice Recognition
houndify; Houndify API
"""
SPEECH_RECOGNITION = True
SPEECH_GENERATION = True

RECOGNITION_ENGINE = 'wit'

GOOGLE_CLOUD_SPEECH_CREDENTIALS = r"""INSERT THE CONTENTS OF THE GOOGLE CLOUD SPEECH JSON CREDENTIALS FILE HERE"""
WIT_AI_KEY = "DSUL4RNT24VADKD4SUEHA55KVIVU4REN"  # Wit.ai keys are 32-character uppercase alphanumeric strings
BING_KEY = "INSERT BING API KEY HERE"  # Microsoft Bing Voice Recognition API keys 32-character lowercase hexadecimal strings
HOUNDIFY_CLIENT_ID = "INSERT HOUNDIFY CLIENT ID HERE"  # Houndify client IDs are Base64-encoded strings
HOUNDIFY_CLIENT_KEY = "INSERT HOUNDIFY CLIENT KEY HERE"  # Houndify client keys are Base64-encoded strings

r = sr.Recognizer()


def eliza_speak(message):
    print(message)

    if SPEECH_GENERATION:
        mp3file = 'speech/%s.mp3' % slugify(unicode(message))

        if not os.path.exists(mp3file):
            tts = gTTS(text=message, lang='en', slow=False)
            tts.save(mp3file)

        mixer.init()
        mixer.music.load(mp3file)
        mixer.music.play()


def speech_recognize(audio_source):
    if RECOGNITION_ENGINE == 'sphinx':
        return r.recognize_sphinx(audio_source)

    if RECOGNITION_ENGINE == 'google_cloud':
        return r.recognize_google_cloud(audio_source, credentials_json=GOOGLE_CLOUD_SPEECH_CREDENTIALS)

    if RECOGNITION_ENGINE == 'wit':
        return r.recognize_wit(audio_source, key=WIT_AI_KEY)

    if RECOGNITION_ENGINE == 'bing':
        return r.recognize_bing(audio_source, key=BING_KEY)

    if RECOGNITION_ENGINE == 'houndify':
        return r.recognize_houndify(audio_source, client_id=HOUNDIFY_CLIENT_ID, client_key=HOUNDIFY_CLIENT_KEY)


def audio_input(prompt_text):
    with sr.Microphone() as source:
        # eliza_speak(prompt_text)
        print prompt_text
        audio = r.listen(source)

    try:
        return speech_recognize(audio)

    except sr.UnknownValueError:
        return "I could not understand what you said."

    except sr.RequestError as e:
        return "Error; {0}".format(e)


def reflect(fragment):
    tokens = fragment.lower().split()
    for i, token in enumerate(tokens):
        if token in reflections:
            tokens[i] = reflections[token]
    return ' '.join(tokens)


def analyze(statement):
    for pattern, responses in psychobabble:
        match = re.match(pattern, statement.rstrip(".!"))
        if match:
            response = random.choice(responses)
            return response.format(*[reflect(g) for g in match.groups()])


def main():
    eliza_speak("Hello. How are you feeling today?")

    while True:
        if SPEECH_RECOGNITION:
            statement = audio_input("> ")

        else:
            statement = raw_input("> ")

        eliza_speak(analyze(statement))

        if re.match(r'quit|bye|goodbye|good\sbye|so\slong|farewell', statement.rstrip(".!")):  # statement == "quit":
            break


if __name__ == "__main__":
    main()
