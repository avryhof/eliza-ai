A very simple A.I.-like Script based on [ELIZA](https://en.wikipedia.org/wiki/ELIZA).

* Support for Speech Recognition
* Support for Text To Speech

TO-DO:

* Google Assistant integration - "Eliza, ask Google"
* Amazon Alexa integration - "Eliza, ask Alexa"
* Possible integration with others